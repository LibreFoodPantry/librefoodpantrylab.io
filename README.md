# librefoodpantry.org.io

Website for LibreFoodPantry (https://librefoodpantry.org).

## Developing

Build website and start local web-server, and watch the log...

```bash
docker-compose up -d && docker-compose logs -f
```

... until you see something like ...

```bash
web_1  | Starting the development server...
web_1  | ℹ Compiling Client
web_1  | ℹ ｢wds｣: Project is running at http://0.0.0.0:80/
web_1  | ℹ ｢wds｣: webpack output is served from /website/
web_1  | ℹ ｢wds｣: Content not from webpack is served from /docusaurus/LibreFoodPantry
web_1  | ℹ ｢wds｣: 404s will fallback to /index.html
web_1  | Browserslist: caniuse-lite is outdated. Please run:
web_1  | npx browserslist@latest --update-db
web_1  | ✔ Client: Compiled successfully in 21.96s
```

... then open a browser to `http://localhost/`.

Stop the server...

```bash
docker-compose down
```

---
Copyright &copy; 2021 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
