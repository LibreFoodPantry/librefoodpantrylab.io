const simplePlantUML = require("@akebifiky/remark-simple-plantuml");

module.exports = {
  title: 'LibreFoodPantry',
  tagline: 'A community building free and open source software for food pantries.',
  url: 'https://librefoodpantry.org',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  favicon: 'img/favicon.ico',
  organizationName: 'LibreFoodPantry', // Usually your GitHub org/user name.
  projectName: 'Website', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'LibreFoodPantry',
      logo: {
        alt: 'LibreFoodPantry Logo',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          to: 'docs/projects',
          label: 'Projects',
          position: 'left'
        },
        {
          to: 'docs/acknowledgements',
          label: 'Acknowledgements',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/librefoodpantry',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {
      copyright: `Copyright © ${new Date().getFullYear()} LibreFoodPantry Authors. This work is licensed
      under the Creative Commons Attribution-ShareAlike 4.0 International License.
      To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/">http://creativecommons.org/licenses/by-sa/4.0/</a>.`
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          // It is recommended to set document id as docs home page (`docs/` path).
          homePageId: 'mission',
          sidebarPath: require.resolve('./sidebars.js'),
          remarkPlugins: [simplePlantUML],
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/LibreFoodPantry/librefoodpantry.gitlab.io/-/blob/main/LibreFoodPantry/'
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/librefoodpantry/website/edit/master/LibreFoodPantry/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
