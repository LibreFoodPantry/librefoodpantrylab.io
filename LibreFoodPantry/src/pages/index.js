import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';

const mission = {
  title: <>Mission</>,
  imageUrl: 'img/undraw_navigator_a479.svg',
  description: (
    <>
      We strive to support local food pantries with quality, free and open source software (FOSS) to help them serve their guests.
    </>
  ),
  learnMoreUrl: 'docs/',
};

const codeOfConduct = {
  title: <>Code of Conduct</>,
  imageUrl: 'img/undraw_community_8nwl.svg',
  description: (
    <>
      We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community.
    </>
  ),
  learnMoreUrl: 'docs/code-of-conduct',
};

const values = {
  title: <>Values</>,
  imageUrl: 'img/undraw_road_sign_mfpo.svg',
  description: (
    <>
      Our values provide a common philosophy around developing software,
      and help us when making decisions.
    </>
  ),
  learnMoreUrl: 'docs/values',
};

const licensing = {
  title: <>Licensing</>,
  imageUrl: 'img/undraw_certification_aif8.svg',
  description: (
    <>
      We license all our code under <a href='gpl-3.0.txt'>GPL-3.0</a> and
      all other content under <a href='cc-by-sa-4.0.txt'>CC-BY-SA-4.0</a>.
    </>
  ),
  learnMoreUrl: 'docs/licensing',
};

const status = {
  title: <>Status</>,
  imageUrl: 'img/undraw_under_construction_46pa.svg',
  description: (
    <>
      We have built prototypes of specific features for specific clients.
      As of yet, no software has been deployed for a client.
      This means we are in the very early phases of development.
      Much about our software, processes, and tools are in flux.
      Please be prepared for change.
    </>
  ),
};

const userStoryMap = {
  title: <>User Story Map</>,
  imageUrl: 'img/undraw_prioritise_tgdh.svg',
  description: (
    <>
      We have a user story map that aggregates features needed by
      one or more of our clients.
    </>
  ),
  learnMoreUrl: 'docs/user-story-map',
};


const coordinatingCommittee = {
  title: <>Coordinating Committee</>,
  imageUrl: 'img/undraw_team_ih79.svg',
  description: (
    <>
      We have a Coordinating Committee that meets weekly to coordinate our efforts.
    </>
  ),
  learnMoreUrl: 'docs/coordinating-committee',
};

const gitLab = {
  title: <>GitLab</>,
  imageUrl: 'img/undraw_scrum_board_cesn.svg',
  description: (
    <>
      We use GitLab projects to house our efforts,
      and we use its issues and issue boards to manage meetings and coordinate work.
    </>
  ),
  learnMoreUrl: 'https://gitlab.com/LibreFoodPantry',
  learnMoreText: 'Visit us on GitLab'
};

const discord = {
  title: <>Discord</>,
  imageUrl: 'img/undraw_Group_chat_unwm.svg ',
  description: (
    <>
      If you have a question, need help, or want to get involved,
      this is a good place to start.
    </>
  ),
  learnMoreUrl: 'https://discord.com/invite/PRth8YK',
  learnMoreText: 'Chat on Discord'
};


const features = [
  mission,
  values,
  codeOfConduct,
  licensing,
  coordinatingCommittee,

  status,
  userStoryMap,
  discord,
  gitLab
];

function Feature({imageUrl, title, description, learnMoreText, learnMoreUrl}) {
  const imgUrl = useBaseUrl(imageUrl);
  const lmt = learnMoreText || "Learn more";
  return (
    <div className={clsx('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
      {learnMoreUrl && (
        <div><Link to={learnMoreUrl}>{lmt}</Link></div>
      )}
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description={`${siteConfig.tagline}`}>
      <header className={clsx('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <div className="row">
            <div className={clsx('col col--6 col--offset-1', styles.feature)}>
              <h1 className="hero__title">{siteConfig.title}</h1>
              <p className="hero__subtitle">{siteConfig.tagline}</p>
              <div className={styles.buttons}>
              <Link
                className={clsx(
                  'button button--outline button--secondary button--lg',
                  styles.getStarted,
                )}
                to={useBaseUrl('docs/')}>
                Get Started
              </Link>
            </div>
            </div>
            <div className={clsx('col col--4', styles.feature)}>
              <img className={styles.featureImage} src="img/logo.svg" alt="LibreFoodPantry logo" />
            </div>
          </div>
        </div>
      </header>
      <main>
        {features && features.length > 0 && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
