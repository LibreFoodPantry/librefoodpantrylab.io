module.exports = {
  someSidebar: [
    {
      type: 'category',
      label: 'Overview',
      items: [
        'mission',
        'values',
        'code-of-conduct',
        'licensing',
        'communication',
        'organization',
        'coordinating-committee',
        'shops',
        'becoming-a-shop-manager',
        'educators',
      ]
    },
    {
      type: 'category',
      label: 'Development',
      items: [
        'contributing',
        'user-story-map',
        'projects',
        'commits-and-their-messages',
        'issue-tracker',
        'ci-cd',
        'shop-setup',
        'release-procedure',
        'generating-the-authors-file',
        'no-fork-workflow',
      ]
    },
    'acknowledgements'
  ]
};
