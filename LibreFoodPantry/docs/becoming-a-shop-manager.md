---
id: becoming-a-shop-manager
title: Becoming a Shop Manager
sidebar_label: Becoming a Shop Manager
---

To become a shop manager, petition the coordinating committee for membership.

**Shop Manager Petition contents**
* Full name
* Email
* Organization
* Which food pantry do you want to develop software for?
* List the systems, features, or components that you propose your Shop would work on.
* The composition, structure, and organization of the shop (number of developers, experience of developers, frequency of meetings, and the term of the shop (e.g., number of semesters, weeks, etc.))
* Agree to uphold the values of LibreFoodPantry
* Agree to regularly attend coordinating committee meetings
* Agree to join and participate in community communication channels -- currently:
    * librefoodpantry-coordinating-committee@googlegroups.com
    * librefoodpantry@googlegroups.com
    * LibreFoodPantry Discord server
* If the existing meeting time for the coordinating committee does not work for the individual, a list of days and times that would.

The coordinating committee reviews the petition and determines whether to appoint the applicant as a shop manager.
