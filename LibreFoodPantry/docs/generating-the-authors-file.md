---
id: generating-the-authors-file
title: Generating the AUTHORS File
sidebar_label: Generating the AUTHORS File
---

:::caution
This document is out of date, but may not be entirely obsolete.
:::

:::caution
This document may not apply to every shop. Please talk to your shop manager to determine if this document applies to your shop.
:::


Use `list-authors.py` to generate the AUTHORS file as follows.

```bash
$ bin/list-authors.py > AUTHORS
```

`list-authors.py` generates the list of authors based on `Author`
and `Co-Authored-By` lines in the project's commit messages.
