---
id: values
title: Values
sidebar_label: Values
---

To ensure a healthy and safe environment in which to collaborate and learn, and to help establish and promote effective development practices, we have adopted the following values. We expect all community members to read and uphold these values.

* [Code of conduct](code-of-conduct)
* [Agile values and principles](https://agilemanifesto.org/)
* [FOSSisms](https://opensource.com/education/14/6/16-foss-principles-for-educators)
