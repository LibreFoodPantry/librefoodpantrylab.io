---
id: shops
title: Shops
sidebar_label: Shops
---

A Shop is a group of developers led by a Shop Manager with a charge to work on a subset of LFP.

## Shop Managers

A Shop Manager is a member of the of their Shop and a member of the LFP Coordinating Committee. A Shop Manager is responsible for the activities of their Shop, has elevated privileges over LFP resources relevant to their Shop's charge, and has the ability to grant elevated privileges to those same LFP resources to their Shop members.

## Shop Members

Members of a Shop answer to the Shop Manager, and carry out development activities within the Shop's charge. Shop members are typically organized into one or more teams.

## A Shop's Charge

A Shop is given authority and responsibility over some part (solutions, systems, features, and/or components) of LFP. Ideally charges of different shops charges do not overlap. Charges are coordinated by the Coordinating Committee.

A Shop may further subdivide and delegate its authority over solutions, systems, features, and/or components to teams within the Shop.
