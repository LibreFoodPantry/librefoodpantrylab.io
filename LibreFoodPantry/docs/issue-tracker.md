---
id: issue-tracker
title: Issue Tracker
sidebar_label: Issue Tracker
---

## Issues

Most of the communication and work for LibreFoodPantry projects should be done
through the
[LFP issue tracker](https://gitlab.com/groups/LibreFoodPantry/-/issues)
by creating, commenting, and working on issues. Using the issue tracker helps
us to maintain a history of what work has and is being done, avoids redundancy,
keeps everyone on the same page, and promotes transparency since everything is
publicly viewable. New issues can easily be created at any time, so if you see
something that needs one, feel free to create one.

## Labels

Labels are used to classify work that needs to be done, organize boards, and help teams coordinate.

At the top-level, LibreFoodPantry defines the `Type::*` family of labels. See
the [LibreFoodPantry Labels page](https://gitlab.com/groups/LibreFoodPantry/-/labels).
These classify work to help us prioritize and process issues.

Shops may define additional labels to help them coordinate. In general, labels
should be defined as low in the Group/Project hierarchy as possible.

:::note
Scoped labels have two colons and have the form `Key::Value`. Each issue can only have one `Value` for each `Key`. For example, given scoped labels `Type::Bug` and `Type::Feature`, an issue can only be labeled with one of these labels.
:::
