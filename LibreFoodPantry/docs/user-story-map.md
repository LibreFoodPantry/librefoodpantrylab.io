---
id: user-story-map
title: User Story Map
sidebar_label: User Story Map
---

Based on interviews with our clients, we have constructed a user story map. For more information on user story maps, see [The New User Story
Backlog is a Map](https://www.jpattonassociates.com/the-new-backlog/). This gives a high level vision for a unified project.

[![Story Map](https://gitlab.com/LibreFoodPantry/StoryMap/raw/master/docs/product-story-map.png)](https://gitlab.com/LibreFoodPantry/StoryMap/raw/master/docs/product-story-map.png ":ignore")

We do not necessarily believe that we will ever implement all of these stories. The purpose of this is to help us select for elaboration and implementation those features that are most-important to our clients.