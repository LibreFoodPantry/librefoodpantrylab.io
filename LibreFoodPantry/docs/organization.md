---
id: organization
title: Organization
sidebar_label: Organization
---

The solutions produced by LibreFoodPantry (LFP) is produced primarily by instructors and their students. So the organization of LFP was designed with this in mind. An instructor who wants their class to participate in LFP becomes an LFP Shop Manager, their class become an LFP Shop, and the Shop is organized into one or more Teams.

```plantuml
@startwbs
* Coordinating Committee
** Shop1
*** Team1
*** Team2
** Shop2
*** Team3
*** Team4
*** Team5
** Shop3
*** Team6
@endwbs
```

When an instructor becomes a Shop Manager, they also join the Coordinating Committee. The Coordinating Committee meets once a week, and has the following responsibilities.

* Guide the direction of LibreFoodPantry.
* Manage the User Story Map, representing high-level requirements.
* Support Shop Managers.
  * Provide advice and guidance to Shop Managers.
  * Allocate resources and privileges to Shop Managers.
  * Coordinate efforts between Shops.
* When appropriate, set policy.

The specific organization of a Shop is up to its Shop Manager.
